import { Separator } from "@/components/ui/separator";
import { useNavigate } from "react-router-dom";
import {
  Menubar,
  MenubarContent,
  MenubarItem,
  MenubarMenu,
  MenubarSeparator,
  MenubarShortcut,
  MenubarTrigger,
} from "@/components/ui/menubar";
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "@/components/ui/tooltip";

import "../App.css";
import { TiHome } from "react-icons/ti";

function Navbar() {
  function handleOpenWindow() {
    console.log("open window!");
  }
     const navigate = useNavigate();
     const handleNavigate_home = () => {
       navigate("/");
     };
  return (
    <>
      <div className="gird place-items-center m-2">
        <Menubar className="flex justify-between">
          <MenubarMenu>
            <TooltipProvider>
              <Tooltip>
                <TooltipTrigger>
                  <MenubarTrigger onClick={handleNavigate_home}>
                    <h1 className="text-gray-600 p-3 text-2xl font-bold m-3 cursor-pointer">
                      ChamaApp
                    </h1>
                    {/* <TiHome size={25} cursor={"pointer"} color="#333" /> */}
                  </MenubarTrigger>
                </TooltipTrigger>
                <TooltipContent>
                  <p>Home</p>
                </TooltipContent>
              </Tooltip>
            </TooltipProvider>
          </MenubarMenu>
          <div className="flex flex-row ">
            <MenubarMenu>
              <MenubarTrigger className="cursor-pointer">
                Reports
              </MenubarTrigger>
              <MenubarContent>
                <MenubarItem>
                  New Tab <MenubarShortcut>⌘T</MenubarShortcut>
                </MenubarItem>
                <MenubarItem onClick={handleOpenWindow}>New Window</MenubarItem>
                <MenubarSeparator />
                <MenubarItem>Share</MenubarItem>
                <MenubarSeparator />
                <MenubarItem>Print</MenubarItem>
              </MenubarContent>
            </MenubarMenu>
            <Separator orientation="vertical" />
            <MenubarMenu>
              <MenubarTrigger className="cursor-pointer">
                Table Banking
              </MenubarTrigger>
              <MenubarContent>
                <MenubarItem>
                  New Tab <MenubarShortcut>⌘T</MenubarShortcut>
                </MenubarItem>
                <MenubarItem>New Window</MenubarItem>
                <MenubarSeparator />
                <MenubarItem>Share</MenubarItem>
                <MenubarSeparator />
                <MenubarItem>Print</MenubarItem>
              </MenubarContent>
            </MenubarMenu>
            <Separator orientation="vertical" />
            <MenubarMenu className="ml-4">
              <MenubarTrigger className="ml-4 cursor-pointer">
                Options...
              </MenubarTrigger>
              <MenubarContent>
                <MenubarItem>
                  New Tab <MenubarShortcut>⌘T</MenubarShortcut>
                </MenubarItem>
                <MenubarItem>New Window</MenubarItem>
                <MenubarSeparator />
                <MenubarItem>Share</MenubarItem>
                <MenubarSeparator />
                <MenubarItem>Print</MenubarItem>
              </MenubarContent>
            </MenubarMenu>
          </div>
        </Menubar>
      </div>
    </>
  );
}

export default Navbar;
