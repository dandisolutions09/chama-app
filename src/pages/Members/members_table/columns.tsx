"use client"
import { useNavigate } from "react-router-dom";
import { ColumnDef } from "@tanstack/react-table"
import { ArrowUpDown, MoreHorizontal } from "lucide-react";
import { Button } from "@/components/ui/button";
import { Checkbox } from "@/components/ui/checkbox";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";

// This type is used to define the shape of our data.
// You can use a Zod schema here if you want.
// export type Payment = {
//   id: string
//   amount: number
//   status: "pending" | "processing" | "success" | "failed"
//   email: string
// }


  //  const member: any = {
  //    id: index,
  //    name: element.name,
  //    totalLoans: totalAmountBorrowed.toFixed(2),
  //    totalSaved: totalAmountSaved.toFixed(2),
  //    //createdAt: element.created_at,
  //  };




export type Member_Internal = {
  id: any;
 // name: any;
  date: any;
  amountSaved: any;
};

export const columns: ColumnDef<Member_Internal>[] = [
  {
    accessorKey: "date",
    header: "Date",
  },
  {
    accessorKey: "amount_saved",
    header: ({ column }) => {
      return (
        <Button
          variant="ghost"
          onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
        >
          Amount saved
          <ArrowUpDown className="ml-2 h-4 w-4" />
        </Button>
      );
      //  <DataTableColumnHeader column={column} title="Email" />;
    },
  },

  {
    accessorKey: "Amount Borrowed",
    header: ({ column }) => {
      return (
        <Button
          variant="ghost"
          onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
        >
          Total Savings
          <ArrowUpDown className="ml-2 h-4 w-4" />
        </Button>
      );
      //  <DataTableColumnHeader column={column} title="Email" />;
    },
  },

  {
    id: "actions",
    cell: ({ row }) => {
      const payment = row.original;
      const navigate = useNavigate();
      const handleNavigate_member = (row) => {
        navigate("/member");
      };

      return (
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button variant="ghost" className="h-8 w-8 p-0">
              <span className="sr-only">Open menu</span>
              <MoreHorizontal className="h-4 w-4" />
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end">
            <DropdownMenuLabel>Actions</DropdownMenuLabel>
            <DropdownMenuItem onClick={handleNavigate_member}>
              View Member
            </DropdownMenuItem>
            <DropdownMenuSeparator />
            <DropdownMenuItem>Add Member Saving</DropdownMenuItem>
            <DropdownMenuItem>View payment details</DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>
      );
    },
  },

  {
    id: "select",
    header: ({ table }) => (
      <Checkbox
        checked={
          table.getIsAllPageRowsSelected() ||
          (table.getIsSomePageRowsSelected() && "indeterminate")
        }
        onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
        aria-label="Select all"
      />
    ),
    cell: ({ row }) => (
      <Checkbox
        checked={row.getIsSelected()}
        onCheckedChange={(value) => row.toggleSelected(!!value)}
        aria-label="Select row"
      />
    ),
    enableSorting: false,
    enableHiding: false,
  },
];
