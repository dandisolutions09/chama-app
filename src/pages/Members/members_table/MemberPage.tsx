import React, { useState } from "react";
import { DataTable } from "./dataTable";
import { Member_Internal, columns } from "./columns";
import { useLocation } from "react-router-dom";

function MemberPage() {
  const [data, setData] = useState<Member_Internal[]>([]);
  const [memberAnalytics, setMemberAnalytics] = useState<any>([]);
  const location = useLocation();

  // console.log("incoming x", location.state.savings)
  //setData(location.state)

  //  location.state.savings.forEach()
          const member: any[] = [];


  location.state.savings.forEach((element: any, index: number) => {
    //console.log("d", element);

    const obj = {
      id: index,
      amount_saved: element.amount_saved,
      date: element.created_at,
    };

   // console.log("obj", obj);
    member.push(obj)

  });

  console.log("member array", member)
 // setMemberAnalytics(member);

  // function handleGetAllMembers() {
  //   console.log("handle get all nurses called!");
  //   //fetch("http://localhost:8080/get-members")
  //   fetch("https://chamaapp.onrender.com/get-members")
  //     .then((response) => response.json())
  //     .then((json) => {
  //       console.log("received members", json);
  //       // json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
  //       json.sort(
  //         (a: { created_at: string }, b: { created_at: string }) =>
  //           new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
  //       );

  //       //  setRows(json);

  //       const members: any[] = [];

  //       json.forEach((element: any, index: number) => {
  //         console.log("element--", element);

  //         const totalAmountBorrowed: number = element.loans.reduce(
  //           (total: any, loan: any) => {
  //             return total + parseFloat(loan.amount_borrowed);
  //           },
  //           0
  //         );

  //         const totalAmountSaved: number = element.savings.reduce(
  //           (total: any, saving: any) => {
  //             return total + parseFloat(saving.amount_saved);
  //           },
  //           0
  //         );

  //         // console.log(
  //         //   "Total Amount Borrowed: $" + totalAmountBorrowed.toFixed(2)
  //         // );

  //         const member: any = {
  //           id: index,
  //           name: element.name,
  //           totalLoans: totalAmountBorrowed.toFixed(2),
  //           totalSaved: totalAmountSaved.toFixed(2),
  //           //createdAt: element.created_at,
  //         };

  //         console.log("member", member);

  //         members.push(member);
  //       });

  //       console.log("members array", members);
  //       setMembers(members);
  //     });
  // }

  return (
    <div className="container mx-auto py-10">
      <div className="flex flex-row text-center justify-between gap-12 mb-4 ">
        <h1 className="text-lg">
          Member : <span className="font-bold text-2xl">{location.state.name}</span>
        </h1>
        <div className="p-4 bg-gray-800 rounded-md text-gray-100 min-w-[200px] justify-start text-start flex flex-col gap-4 shadow-lg">
          <h1 className="text-3xl font-bold">{location.state.totalSaved} </h1>
          <p>Savings</p>
        </div>
      </div>

      <DataTable columns={columns} data={member} />
    </div>
  );
}

export default MemberPage;
