// to me
import React, { useEffect, useState } from "react";
import { Payment, columns } from "./columns";
//import { DataTable } from "./data-table";
import { nanoid } from "nanoid";
import { DataTable } from "./dataTable";

function DemoPage() {
  const [data, setData] = useState<Payment[]>([]);
  const [members, setMembers] = useState<any>([]);

  function handleGetAllMembers() {
    console.log("handle get all nurses called!");
    //fetch("http://localhost:8080/get-members")
    fetch("https://chamaapp.onrender.com/get-members")
      .then((response) => response.json())
      .then((json) => {
        console.log("received members", json);
        // json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
        json.sort(
          (a: { created_at: string }, b: { created_at: string }) =>
            new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
        );

        //  setRows(json);

        const members: any[] = [];

        json.forEach((element: any, index: number) => {
          console.log("element--", element);

          const totalAmountBorrowed: number = element.loans.reduce(
            (total: any, loan: any) => {
              return total + parseFloat(loan.amount_borrowed);
            },
            0
          );

          const totalAmountSaved: number = element.savings.reduce(
            (total: any, saving: any) => {
              return total + parseFloat(saving.amount_saved);
            },
            0
          );

          // console.log(
          //   "Total Amount Borrowed: $" + totalAmountBorrowed.toFixed(2)
          // );

          const member: any = {
            id: index,
            name: element.name,
            totalLoans: totalAmountBorrowed.toFixed(2),
            totalSaved: totalAmountSaved.toFixed(2),
            savings: element.savings,
            loans: element.loans,
            //createdAt: element.created_at,
          };

          console.log("member", member);

          members.push(member);
        });

        console.log("members array", members);
        setMembers(members);
      });
  }

  useEffect(() => {
   

    handleGetAllMembers();
  }, []); // Empty dependency array ensures the effect runs once on mount


  return (
    <div>
      <DataTable columns={columns} data={members} />
    </div>
  );
}

export default DemoPage;
