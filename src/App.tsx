import React from "react";
import Navbar from "./new_components/Navbar";
import DemoPage from "./payments/page";

import { BrowserRouter, Routes, Route, Link, NavLink } from "react-router-dom";
import MemberPage from "./pages/Members/members_table/MemberPage";

export default function App() {
  return (
    <>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<DemoPage />} />
          <Route path="member" element={<MemberPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}
